# Project 1 template #

This is the template for your project 1 submission. For a full description of the Project, please refer to [this document](https://docs.google.com/document/d/1WhCeZpnAJyuraJI2cAsU8thabzHej7SHa_2b4pQv9Ls/edit?usp=sharing).

### Table of content

* [Source code](Project1_Code/)
* [Demo video](Video/)
* Description of projects and notes in README.md (this file)

## How can we play the game? ##
![screenshot] (./img/hamburger.jpg)
If we click start button, the clock goes and the game starts.
The play time is 60 seconds.
On the top, text pops up telling me which burger to make. And I have to make it by stacking correct ingredients.
You can stack ingredients by clicking the buttons on the bottom.
If you are finished making a burger, you can click "FINISH" button. If you have correctly made the burger, you will be paid for your burger and the total money you earn will increase. However if you have made it wrong, it will not give you any money.
If you don't memorize how to make a burger, you can click "HINT" button to get help of the recipe.
However, you will have to pay 300 from the total money you have earned.

After 60 seconds has passed, you can not make any more burgers.
You can restart by clicking "START" button.

Aim to get the highest score! Enjoy!

### Classes in this Program ###
- Burger
    - addIngredient : used to add ingredients so that we can use draw() function
    - numIngredient : needed for future implications of Customer and calories.
    - getRecipe : Ingredient array list of string to compare ingredients.
    - getName : name of burger in string to print on top of program
    - getHintRecipe : Used for draw() when hint is needed
- Burgerfactory
    - Factory for making different kinds of burgers.
    - Each has unique Ingredients
- Button
    - Button class needed for 3 buttons of our program : Start, Finish, Hint
    - isOver() to check which button was clicked
- Food
    - Used for ingredients
    - setters and getters are implemented. Setters are used in FoodFactory and getters in setup() and draw()
    - calory is for later implication of Customer and Calories feature.
- FoodFactory
    - Making all ingredients for burger
    - we set the price and calory of each ingredient using FoodFactory
- Customer
    - To be made later on ( Time Issue)
    - Actual intention was to create type of Customer
        - one who wants a specific type of burger
        - one who wants a burger at specific price range
        - one who wants a burger at specific calory range
        - one who wants a silly, random burger (like no buns, only sauces added, etc)
