/*
	Project 1
	Name of Project: Tower Defense
	Author: Jung Jae Lee 20140441
	Date: 2020.05.31
*/

ArrayList<String> burgermenu = new ArrayList<String>();
ArrayList<Food> burgertotal = new ArrayList<Food>();
ArrayList<Food> hintrecipe = new ArrayList<Food>();
ArrayList<Food> t = new ArrayList<Food>();
ArrayList<Burger> allburger = new ArrayList<Burger>();
ArrayList<String> burgertotalstring = new ArrayList<String>();
FoodFactory ff;
BurgerFactory bf;
Button fb;
Button hb;
Button sb;
PImage main;
int earn;
int totalearn;
int time;
int starttime = 0;
int playtime = 60000;
int randomnumber = 100;
boolean start;
boolean hint;
void setup()
{
	// your code
	size(600,800);
	main = loadImage("hamburger.jpg");  //load main Image
	
	ff = new FoodFactory();   // adding all ingredients into total ingredient list so that we can get data when an ingredient button is pressed
	t.add(ff.getTopBun());
	t.add(ff.getMidBun());
	t.add(ff.getBotBun());
	t.add(ff.getBeefPatty());
	t.add(ff.getCheese());
	t.add(ff.getChicken());
	t.add(ff.getPotato());
	t.add(ff.getLettuce());
	t.add(ff.getTomato());
	t.add(ff.getPickle());
	t.add(ff.getOnion());
	t.add(ff.getMustard());
	t.add(ff.getKetchup());
	t.add(ff.getMayo());
	t.add(ff.getBarbeque());
	
	fb = new Button(540,532.5);    // creating 3 buttons for program run
	hb = new Button(540,582.5);
	sb = new Button(45,22.5);

	bf = new BurgerFactory();				// adding all burger menus to all burger list
	allburger.add(bf.makeCheeseBurger());
	allburger.add(bf.makeBigMac());
	allburger.add(bf.makeMcSpicy());
	allburger.add(bf.makeHashPot());
	allburger.add(bf.makeBulgogi());
	allburger.add(bf.makeDoubleBulgogi());
	allburger.add(bf.make1955());
}

void draw()
{
	// your code
	image(main,0,0);
	int i = 440;			// position where burger we make is going to be placed (where the dish is)
	int j = 400;			// position where the hint is goint to be placed on the right side	
	if(start == true) {				//implemented this to get the time when the start button is clicked
		starttime = millis();
		start = false;				//We have to get starttime only once when start button is clicked
	}

	fill(0);
	textSize(20);
	text("Total money earned : " + totalearn,width/2-150,30);			// text to show total money earned
	try{
		text("Make : " + allburger.get(randomnumber).getName(),width/2-150,80);
		if(starttime+playtime-millis()>0) text((starttime + playtime-millis())/1000,550,30);			//only show timer when we have time left. It disappears when below 0

		}catch(IndexOutOfBoundsException e){}			//At first, when we don't click start button. random number is so big that it is out of range. Then exception occurs
														//Exception occuring means game has not started so playtime and burger to make does not appear on screen
	if(millis() < starttime + playtime){				//when we are within playtime
		for(Food food:burgertotal){						//add ingredient we click to the burgertotal list and then draw it
			food.draw(272,i);
			i = i - 8;
			}
		}
	if (hint == true){															//When hint button is pressed, we draw the answer ingredient with more space for easy viewing
		for(Food food : allburger.get(randomnumber).getHintRecipe()){
			food.draw(500,j);
			j = j -15;
		}
	}
}
void mousePressed()
{
	for(Food f:t)										//for all ingredient buttons, if it is clicked, it is added to burgertotal and burgertotalstring list
	{
		if(f.isOver()) {
			burgertotal.add(f);
			burgertotalstring.add(f.getString());		//list used for comparison
		}
	}
	if(fb.isOver())										//when finish button is clicked
	{	
		if(millis()< starttime + playtime){														//if the answer is correct, we add total money earned. 
			if(allburger.get(randomnumber).getRecipe().equals(burgertotalstring)){				// reset the list for next burger we have to make
				for(Food f:burgertotal) totalearn += f.getsellPrice();							// hint to false so that when we click finish, the hint diagram disappears
				burgertotal = new ArrayList<Food>();
				burgertotalstring = new ArrayList<String>();
				randomnumber = int(random(0,6.99));
				hint = false;
			}
			else{
				randomnumber = int(random(0,6.99));
				burgertotal = new ArrayList<Food>();
				burgertotalstring = new ArrayList<String>();
				hint = false;
			}
		}
	}
	if(sb.isOver())									//when start button is clicked
	{												//start = true so that we can end exception from above		
		start = true;								//random number assigned to get a burger we have to make
		randomnumber = int(random(0,6.99));			//total money earned have to be reset when a new game is started
		totalearn = 0;
	}

	if(hb.isOver())									//when hint button is created
	{
		hint = true;								//enable this so that hint diagram is drawn
		totalearn -= 300;							//penalty for using hint
	}

}

class Customer
{
	ArrayList<String> cust = new ArrayList<String>();
}

class Burger
{
	ArrayList<String> recipe = new ArrayList<String>();
	ArrayList<Food> hintrecipe = new ArrayList<Food>();
	int n = 0;
	String burgername;
	Burger(String burgername){
		this.burgername = burgername;
	}
	void addIngredient(Food f){
		recipe.add(f.getString());
		hintrecipe.add(f);
		n++;
	}
	int numIngredient(){
		return n;
	}
	ArrayList<String> getRecipe(){							//used for easy comparison of correct answer and what we have made
		return recipe;
	}
	String getName(){										//used to print on top the burger name
		return burgername;
	}
	ArrayList<Food> getHintRecipe(){						//Used for drawing Hint diagram. 
		return hintrecipe;
	}
}

class BurgerFactory											//factory for making different kinds of burgers
{
	Burger makeCheeseBurger(){
		Burger b = new Burger("Cheese Burger");
		b.addIngredient(ff.getBotBun());
		b.addIngredient(ff.getBeefPatty());
		b.addIngredient(ff.getCheese());
		b.addIngredient(ff.getKetchup());
		b.addIngredient(ff.getTopBun());
		return b;
	}

	Burger makeBigMac(){
		Burger b = new Burger("Big Mac");
		b.addIngredient(ff.getBotBun());
		b.addIngredient(ff.getLettuce());
		b.addIngredient(ff.getBeefPatty());
		b.addIngredient(ff.getPickle());
		b.addIngredient(ff.getKetchup());
		b.addIngredient(ff.getMidBun());
		b.addIngredient(ff.getLettuce());
		b.addIngredient(ff.getBeefPatty());
		b.addIngredient(ff.getMayo());
		b.addIngredient(ff.getTopBun());
		return b;
	}

	Burger makeMcSpicy(){
		Burger b = new Burger("McSpicy");
		b.addIngredient(ff.getBotBun());
		b.addIngredient(ff.getChicken());
		b.addIngredient(ff.getTomato());
		b.addIngredient(ff.getLettuce());
		b.addIngredient(ff.getMayo());
		b.addIngredient(ff.getTopBun());
		return b;
	}

	Burger makeHashPot(){
		Burger b = new Burger("Hash Brown Burger");
		b.addIngredient(ff.getBotBun());
		b.addIngredient(ff.getBeefPatty());
		b.addIngredient(ff.getLettuce());
		b.addIngredient(ff.getPotato());
		b.addIngredient(ff.getOnion());
		b.addIngredient(ff.getKetchup());
		b.addIngredient(ff.getTopBun());
		return b;
	}
	Burger makeBulgogi(){
		Burger b = new Burger("Bulgogi Burger");
		b.addIngredient(ff.getBotBun());
		b.addIngredient(ff.getBeefPatty());
		b.addIngredient(ff.getLettuce());
		b.addIngredient(ff.getBarbeque());
		b.addIngredient(ff.getTopBun());
		return b;
	}
	Burger makeDoubleBulgogi(){
		Burger b = new Burger("Double Bulgogi Burger");
		b.addIngredient(ff.getBotBun());
		b.addIngredient(ff.getBeefPatty());
		b.addIngredient(ff.getBarbeque());
		b.addIngredient(ff.getBeefPatty());
		b.addIngredient(ff.getLettuce());
		b.addIngredient(ff.getMayo());
		b.addIngredient(ff.getTopBun());
		return b;
	}
	Burger make1955(){
		Burger b = new Burger("1955 Burger");
		b.addIngredient(ff.getBotBun());
		b.addIngredient(ff.getKetchup());
		b.addIngredient(ff.getBeefPatty());
		b.addIngredient(ff.getOnion());
		b.addIngredient(ff.getTomato());
		b.addIngredient(ff.getLettuce());
		b.addIngredient(ff.getMustard());
		b.addIngredient(ff.getTopBun());
		return b;
	}

}

class Button
{
	private float x;
	private float y;
	private float size = 90;
	Button(float x, float y)
	{
		this.x = x;
		this.y = y;
	}
	boolean isOver()
	{
		if(mouseX < x - size/2 || mouseX > x + size/2) return false;
		if(mouseY < y - size/4 || mouseY > y + size/4) return false;
		return true;
	}	
}

class FoodFactory
{
	Food getTopBun()
	{
		Food f;
		f = new Food(55,555,"topbun.png","tb");
		f.setbuyPrice(50);
		f.setsellPrice(100);
		f.setCalories(30);
		return f;
	}
	Food getMidBun()
	{
		Food f;
		f = new Food(55,650,"middlebun.png","mb");
		f.setbuyPrice(50);
		f.setsellPrice(100);
		f.setCalories(30);
		return f;
	}
	Food getBotBun()
	{
		Food f;
		f = new Food(55,745,"bottombun.png","bb");
		f.setbuyPrice(50);
		f.setsellPrice(100);
		f.setCalories(30);
		return f;
	}
	Food getBeefPatty()
	{
		Food f;
		f = new Food(150,555,"beefpatty.png","beef");
		f.setbuyPrice(300);
		f.setsellPrice(1000);
		f.setCalories(200);
		return f;
	}
	Food getChicken()
	{
		Food f;
		f = new Food(150,650,"chicken.png","chk");
		f.setbuyPrice(250);
		f.setsellPrice(900);
		f.setCalories(150);
		return f;
	}
	Food getCheese()
	{
		Food f;
		f = new Food(340,555,"cheese.png","chee");
		f.setbuyPrice(100);
		f.setsellPrice(500);
		f.setCalories(50);
		return f;
	}
	Food getPotato()
	{
		Food f;
		f = new Food(245,555,"hashpotato.png","pot");
		f.setbuyPrice(150);
		f.setsellPrice(500);
		f.setCalories(100);
		return f;
	}
	Food getLettuce()
	{
		Food f;
		f = new Food(340,650,"lettuce.png","lett");
		f.setbuyPrice(100);
		f.setsellPrice(400);
		f.setCalories(50);
		return f;
	}
	Food getOnion()
	{
		Food f;
		f = new Food(435,555,"onion.png","on");
		f.setbuyPrice(50);
		f.setsellPrice(300);
		f.setCalories(50);
		return f;
	}
	Food getPickle()
	{
		Food f;
		f = new Food(435,650,"pickle.png","pic");
		f.setbuyPrice(50);
		f.setsellPrice(200);
		f.setCalories(50);
		return f;
	}
	Food getTomato()
	{
		Food f;
		f = new Food(245,650,"tomato.png","tom");
		f.setbuyPrice(150);
		f.setsellPrice(400);
		f.setCalories(50);
		return f;
	}
	Food getKetchup()
	{
		Food f;
		f = new Food(150,745,"ketchup.png","ket");
		f.setbuyPrice(50);
		f.setsellPrice(150);
		f.setCalories(30);
		return f;
	}
	Food getMustard()
	{
		Food f;
		f = new Food(245,745,"mustard.png","must");
		f.setbuyPrice(50);
		f.setsellPrice(150);
		f.setCalories(30);
		return f;
	}
	Food getMayo()
	{
		Food f;
		f = new Food(435,745,"mayo.png","mayo");
		f.setbuyPrice(50);
		f.setsellPrice(150);
		f.setCalories(30);
		return f;
	}
	Food getBarbeque()
	{
		Food f;
		f = new Food(340,745,"bbq.png","bbq");
		f.setbuyPrice(50);
		f.setsellPrice(200);
		f.setCalories(40);
		return f;
	}
}

class Food
{
	private int buyprice;
	private int sellprice;
	private int calories;
	private int x;
	private int y;
	private String c;
	int buttonsize = 90;
	PImage img;
	Food(int x, int y, String filename, String c){
		this.x = x;
		this.y = y;
		this.c = c;
		img = loadImage(filename);
	}
	void draw(int m,int n)
	{
		image(img,m,n);
	}
	//
	boolean isOver()								
	{
		if(mouseX < x - buttonsize/2 || mouseX > x + buttonsize/2) return false;
		if(mouseY < y - buttonsize/2 || mouseY > y + buttonsize/2) return false;
		return true;
	}
	void setbuyPrice(int b)
	{
		buyprice = b;
	}
	void setsellPrice(int s)
	{
		sellprice = s;
	}
	void setCalories(int c)
	{
		calories = c;
	}
	int getbuyPrice()
	{
		return buyprice;
	}
	int getsellPrice()
	{
		return sellprice;
	}
	int getCalory()
	{
		return calories;
	}
	String getString()
	{
		return c;
	}
}
